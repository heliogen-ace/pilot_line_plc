﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.9">
  <POU Name="ABB_IRC5" Id="{ca7ecba3-54b8-4b64-992f-33e28c18e904}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK ABB_IRC5
VAR_INPUT
	inRobot: ABB_IRC5_HGCore_In; //Input data from the robot
	inSafeReset: BOOL; //Safety Reset pass through
	inResetSeqFault: BOOL; //Reset my bad programming
	inDryCycle: BOOL; //Dry cycle - just a passthrough if implemented
END_VAR
VAR_OUTPUT
	outRobot: ABB_IRC5_HGCore_Out; //Output data to the robot
	outProgramRunning: BOOL;
	outRobotStatus: eRobotState;
	outSeqFault: ABB_IRC5_SequenceFault; //Sequencer Fault, this shouldn't happen if it's tested right
END_VAR
VAR
	_programToRun: USINT;
	_resetRequiredTmr: TOF;
	_guardStopReset: BOOL;
	_step: INT; //Step Sequencer Index
	_resumeFlag: BOOL; //Resume flag to be called by Cycle Start Method
	_motorsOffFlag: BOOL; //Motors off flag triggered by Cycle Stop Method
	_stopAfterInstFlag: BOOL; //Stop type = stop after instruction
	_stepTimer: TON;
	_oldStep: INT;
	_AutoRunningTON: TON; //Shitty way round a bug
	_AutoRunningEntry: BOOL; //Shitty way round a bug
	_AutoRunningEntryBypass: BOOL; //Shittier way around a bug
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////								ABB IRC 5 Heliogen Core Robot Interface											////
////																												////
////								Work in Progress 3/9/2022 - O Ghalebi											////
////																												////
////	Designed around the current HG Core RAPID code where robot is always running and accepts program numbers	////
////	and triggers to start programs																				////
////	This will change, a lot, it's currently written with the PackML state machine in mind but not integrated	////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////								Setup, Passthroughts & Status Determination										////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Safety Reset passthrough
outRobot.out.PN_P2R_1_2_cmdResetEmergencyStop := inSafeReset;

//Timer to determine if a Guard Stop Reset is required
_ResetRequiredTmr(PT:=T#2000ms,Q=>_guardStopReset);
_ResetRequiredTmr.IN := inRobot.in.PN_R2P_0_0_MotorOn AND inRobot.in.PN_R2P_0_1_MotorOff;

//DryCycle Pass Through

outRobot.out.PN_P2R_1_6_cmdDryCycle := inDryCycle;

//Falling edge Timer required on Robot going into Auto Running FROM AutoMotorsOn, robot reports status back as Auto Running (Motors On + Cycle On) before it's actually ready to accept a program command for some reason.
//This means programs triggered immediately after robot is in AutoRunning will not actually execute.
//This bug needs to be traced and killed better but for now this works
_AutoRunningTON(IN:=_AutoRunningEntry,PT:=T#500MS);

IF outRobotStatus <> eRobotState.AutoRunning THEN
	_AutoRunningEntry := FALSE;
END_IF

IF outRobotStatus = eRobotState.AutoRunningProgram THEN
	_AutoRunningEntryBypass := TRUE;
ELSIF outRobotStatus <> eRobotState.AutoRunning THEN
	_AutoRunningEntryBypass := FALSE;
END_IF
//BugFix code <end>

//Status output is determined from robot states
IF inRobot.in.PN_R2P_0_4_AutoOn THEN //Robot is in Auto Mode
	IF inRobot.in.PN_R2P_0_2_MotorOnState THEN //Robot is motors on
		IF inRobot.in.PN_R2P_0_5_CycleOn THEN //Cycle running, i.e. T_ROB1 is executing
			IF inRobot.in.PN_R2P_4_0_ProgramRunning THEN //HG Core program is running
				outRobotStatus := eRobotState.AutoRunningProgram;
			ELSE
				_AutoRunningEntry := TRUE;
				//IF statement are part of bug fix code from above.
				IF _AutoRunningTON.Q OR _AutoRunningEntryBypass THEN
					outRobotStatus := eRobotState.AutoRunning;
					_AutoRunningEntryBypass := FALSE;
				END_IF
			END_IF
		ELSE
			IF inRobot.in.PN_R2P_4_0_ProgramRunning THEN
				outRobotStatus := eRobotState.AutoMotorsOnSuspendedProgram;
			ELSE
				outRobotStatus := eRobotState.AutoMotorsOn;
			END_IF
		END_IF
	ELSE //Motors off
		IF _guardStopReset THEN 
			outRobotStatus := eRobotState.GuardStopResetReq;
		ELSE
			outRobotStatus := eRobotState.Auto;
		END_IF
	END_IF
ELSE //Robot is in Manual or Unknown
	outRobotStatus := eRobotState.Manual;
END_IF

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////								Step Sequencer to handle methods (gross)										////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

_stepTimer(IN:=_step > 0 AND _step = _oldStep,PT:=T#30D);
IF _step <> _oldStep THEN
	_oldStep := _step;
END_IF

CASE _step OF
	0: //Idle, cleanup here
		_resumeflag := FALSE;
		
	////////////////////////////////////////////////////////////////
	////				Cycle Start sequence					////
	////////////////////////////////////////////////////////////////
	
	100: 
		CASE outRobotStatus OF //Find out where the robot is right now, should not have got here if you're not in one of these states
			eRobotState.GuardStopResetReq:
				_step:=110; //Robot is in Guard Stop Reset state, needs a safety reset first
			
			eRobotState.Auto: //Robot is in Auto State, skip there
				_step:=120;
			
			eRobotState.AutoMotorsOn .. eRobotState.AutoMotorsOnSuspendedProgram : //Robot is in Auto with Motors On, skip there
				_step:=130;

		ELSE //Well shit
			_step:=9999;
		END_CASE
	
	110: //Guard Stop Reset Required
		outRobot.out.PN_P2R_1_2_cmdResetEmergencyStop := TRUE;
		IF _stepTimer.ET > T#250MS OR outRobotStatus > eRobotState.GuardStopResetReq THEN
			_step := 111;
		END_IF	
		
	111: //Turn off Guard Stop Reset Required flag
		outRobot.out.PN_P2R_1_2_cmdResetEmergencyStop := FALSE;
		
		CASE outRobotStatus OF //Find out where you are again
			eRobotState.Auto: //Robot is in Auto State, skip there
				_step:=120;
			
			eRobotState.AutoMotorsOn .. eRobotState.AutoMotorsOnSuspendedProgram : //Robot is in Auto with Motors On, skip there
				_step:=130;
		ELSE
			IF _stepTimer.ET > T#1S THEN //Didn't reset, go to fault
				_step:=9990;
			END_IF
		END_CASE
	
	120: //In Auto Mode, Turn Motors On
		outRobot.out.PN_P2R_0_0_cmdMotorOn := TRUE;
		IF _stepTimer.ET > T#250MS OR outRobotStatus > eRobotState.AutoMotorsOn THEN
			_step := 121;
		END_IF	
	121: 
		outRobot.out.PN_P2R_0_0_cmdMotorOn := FALSE;
        
		CASE outRobotStatus OF //Find out where you are again
			eRobotState.AutoMotorsOn .. eRobotState.AutoMotorsOnSuspendedProgram : //Robot is in Auto with Motors On, skip there
				_step:=130;
		ELSE
			IF _stepTimer.ET > T#1S THEN //Didn't go motors on
				_step:=9991;
			END_IF
		END_CASE
	    
        
        
	130: //In Auto Mode Motors On, Check resume flag then PP to main or skip
		IF _resumeFlag = FALSE THEN //If the resume flag is on send PP to Main (Probably need to add the "pointer moved" check here)
			outRobot.out.PN_P2R_0_2_cmdPPToMain := TRUE;
			IF _stepTimer.ET > T#250MS THEN
				_step := 131;
			END_IF
		ELSE
			_step := 131;
		END_IF	
	131: 
		outRobot.out.PN_P2R_0_2_cmdPPToMain := FALSE;
		outRobot.out.PN_P2R_0_4_cmdStart := TRUE;
		_resumeFlag := FALSE;

		CASE outRobotStatus OF //Find out where you are again
			eRobotState.AutoRunning .. eRobotState.AutoRunningProgram: //It's running!
				_resumeFlag := FALSE;
				outRobot.out.PN_P2R_0_4_cmdStart := FALSE;
				_step := 0;
		ELSE
			IF _stepTimer.ET > T#1S THEN //Didn't reset, go to fault
				outRobot.out.PN_P2R_0_4_cmdStart := FALSE;
				_step := 9992;
			END_IF
		END_CASE
	
	////////////////////////////////////////////////////////////////
	////				Cycle Stop sequence					////
	////////////////////////////////////////////////////////////////
	
	200: 
		IF _stopAfterInstFlag THEN
			outRobot.out.PN_P2R_0_7_cmdStopAtEndOfInst := TRUE;
		ELSE
			outRobot.out.PN_P2R_0_5_cmdStop := TRUE;
		END_IF
		
		IF _stepTimer.ET > T#250MS THEN //Keep output on for 250ms
			_step := 210;
		END_IF
		
	210: 
		outRobot.out.PN_P2R_0_7_cmdStopAtEndOfInst := FALSE;
		outRobot.out.PN_P2R_0_5_cmdStop := FALSE;
		
		IF outRobotStatus < eRobotState.AutoRunning THEN
			_stopAfterInstFlag := FALSE;		
			IF _motorsOffFlag THEN
				_step := 220;
			ELSE
				_step := 0;
			END_IF
		END_IF
		
		IF _stepTimer.ET > T#10S THEN //Wait for 10s then fail if not already moved on. This should be configurable because some instructions could take longer but for now fuck it
			_step := 9993;
		END_IF
		
	220:
		outRobot.out.PN_P2R_0_1_cmdMotorOff := TRUE;
		
		IF outRobotStatus < eRobotState.AutoMotorsOn THEN
			_step := 230;
		END_IF
		
		IF _stepTimer.ET > T#250MS THEN //Keep output on for 250ms
			_step := 230;
		END_IF
		
	230:
		outRobot.out.PN_P2R_0_1_cmdMotorOff := FALSE;
		
		IF outRobotStatus < eRobotState.AutoMotorsOn THEN
			_step := 0;
		END_IF
		
		IF _stepTimer.ET > T#10S THEN //Fault Timer
			_step := 9994;
		END_IF
		
	////////////////////////////////////////////////////////////////
	////				Run Program sequence					////
	////////////////////////////////////////////////////////////////
	
	300: 
		outRobot.out.PN_P2R_2_ProgramNumber := _programToRun;
		outRobot.out.PN_P2R_1_5_cmdAckComplete := FALSE;
		outRobot.out.PN_P2R_1_4_cmdTriggerProgram := FALSE;
		_step := 310;
		
	310:
		IF inRobot.in.PN_R2P_2_ProgramNumberEcho = _programToRun THEN
			_step := 320;
		END_IF
		
		IF _stepTimer.ET > T#1S THEN //Wait for program to echo before looking at invalid flag or faulting for any other reason
			IF inRobot.in.PN_R2P_1_7_ProgramInvalid THEN
				_step := 9995; //Faulted due to Invalid Program #
			ELSE
				_step := 9996; //Failed to load program for some other reason
			END_IF		
		END_IF
	
	320:
		IF inRobot.in.PN_R2P_1_6_CycleComplete THEN
			outRobot.out.PN_P2R_1_5_cmdAckComplete := TRUE;
			_step := 330;
		ELSE
			_step := 340;
		END_IF
	
	330:
		
		
		IF inRobot.in.PN_R2P_1_6_CycleComplete = FALSE THEN
			outRobot.out.PN_P2R_1_5_cmdAckComplete := FALSE;
			_step := 340;
		END_IF
	
		IF _stepTimer.ET > T#1S THEN //Wait for 250ms then it failed to ack
			outRobot.out.PN_P2R_1_5_cmdAckComplete := FALSE;
			_step := 9997;
		END_IF
		
	340:
		outRobot.out.PN_P2R_1_4_cmdTriggerProgram := TRUE;
	
		_step := 350;
	
	350:
		
		IF inRobot.in.PN_R2P_4_0_ProgramRunning OR inRobot.in.PN_R2P_1_6_CycleComplete THEN
			//Program either started running or was very quick and is done already
			outRobot.out.PN_P2R_1_4_cmdTriggerProgram := FALSE;
			_programToRun := 0;
			_step := 0;
		END_IF
		
		IF _stepTimer.ET > T#1S THEN //Wait
			outRobot.out.PN_P2R_1_4_cmdTriggerProgram := FALSE;
			_step := 9998;
		END_IF
	
	////////////////////////////////////////////////////////////////
	////				Sequencer Faults >=9000					////
	////////////////////////////////////////////////////////////////
	
	9990: //Guard stop failed to reset
		outSeqFault.fault := TRUE;
		outSeqFault.faultText := '9990: Guard stop failed to reset';
		
		IF inResetSeqFault THEN
			_ResetSeqFault();
		END_IF
	
	9991: //Motors failed to switch on
		outSeqFault.fault := TRUE;
		outSeqFault.faultText := '9991: Motors failed to switch on';
		
		IF inResetSeqFault THEN
			_ResetSeqFault();
		END_IF	
		
	9992: //Failed to Start Cycle
		outSeqFault.fault := TRUE;
		outSeqFault.faultText := '9992: Cycle Start Failed';
		
		IF inResetSeqFault THEN
			_ResetSeqFault();
		END_IF	
	
	9993: //Failed to Stop Cycle
		outSeqFault.fault := TRUE;
		outSeqFault.faultText := CONCAT('9993: Cycle Stop Failed. Cycle Stop After Instruction :=', BOOL_TO_STRING(_stopAfterInstFlag));
		_stopAfterInstFlag := FALSE;
		
		IF inResetSeqFault THEN
			_ResetSeqFault();
		END_IF	
		
	9994: //Failed to Turn Motors Off
		outSeqFault.fault := TRUE;
		outSeqFault.faultText := '9994: Motors Failed To Turn Off';
		
		IF inResetSeqFault THEN
			_ResetSeqFault();
		END_IF	
		
	9995: //Invalid Program #
		outSeqFault.fault := TRUE;
		outSeqFault.faultText := CONCAT('9995: Invalid Program Number := ',USINT_TO_STRING(_programToRun));
		
		IF inResetSeqFault THEN
			_ResetSeqFault();
		END_IF	
		
	9996: //Failed to echo program #
		outSeqFault.fault := TRUE;
		outSeqFault.faultText := '9996: Failed to Echo Program Number';
		
		IF inResetSeqFault THEN
			_ResetSeqFault();
		END_IF	
		
	9997: //Failed to ack program complete
		outSeqFault.fault := TRUE;
		outSeqFault.faultText := '9997: Failed to ack program complete';
		
		IF inResetSeqFault THEN
			_ResetSeqFault();
		END_IF	
		
	9998: //Program Didn't Start
		outSeqFault.fault := TRUE;
		outSeqFault.faultText := '9998: Program Didnt Start';
		
		IF inResetSeqFault THEN
			_ResetSeqFault();
		END_IF	
		
	9999: //Sequencer Fault - HTF did you get here
		outSeqFault.fault := TRUE;
		outSeqFault.faultText := '9999: Shit';
		
		IF inResetSeqFault THEN
			_ResetSeqFault();
		END_IF
END_CASE
]]></ST>
    </Implementation>
    <Method Name="_ResetSeqFault" Id="{20086c95-5e7b-4048-a21f-9a607180723e}">
      <Declaration><![CDATA[METHOD PRIVATE _ResetSeqFault : BOOL
VAR_INPUT
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[inResetSeqFault := FALSE;
outSeqFault.fault := FALSE;
outSeqFault.faultText := '';
_step := 0;]]></ST>
      </Implementation>
    </Method>
    <Method Name="CycleStart" Id="{f498a8e2-27fd-400e-b312-d724a9a08139}">
      <Declaration><![CDATA[METHOD CycleStart : BOOL
VAR_INPUT
	inResume: BOOL;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[//Check if I can be invoked
IF outRobotStatus > eRobotState.Manual AND outRobotStatus < eRobotState.AutoRunning AND _step = 0 THEN
	//Robot is in the right state for a Cycle Start to be called
	_resumeFlag := inResume;
	_step := 100; //Start Cycle Start
	CycleStart := TRUE; //Return that it could be started
ELSE
	CycleStart :=FALSE; //Return false, robot not in right state
END_IF]]></ST>
      </Implementation>
    </Method>
    <Method Name="CycleStop" Id="{d96e81f4-fc30-43c9-8e8a-bb5192d26cee}">
      <Declaration><![CDATA[METHOD CycleStop : BOOL
VAR_INPUT
	inMotorsOff: BOOL;
	inStopAfterInstruction: BOOL;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[//Check if I can be invoked
IF outRobotStatus >= eRobotState.AutoMotorsOn AND outRobotStatus <= eRobotState.AutoRunningProgram AND _step = 0 THEN
	//Robot is in the right state for Cycle Stop to be triggered
	_motorsOffFlag := inMotorsOff;
	_step := 200; //Start Cycle Stop
	CycleStop := TRUE; //Return that it could be started
ELSE
	CycleStop := FALSE; //Return false, robot not in right state
END_IF]]></ST>
      </Implementation>
    </Method>
    <Method Name="ResetSeq" Id="{cce08945-8674-46f8-ae80-cdd703b9d94c}">
      <Declaration><![CDATA[METHOD ResetSeq : BOOL
VAR_INPUT
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[inResetSeqFault := TRUE;]]></ST>
      </Implementation>
    </Method>
    <Method Name="RunProgram" Id="{c8cfac2d-409c-45c4-b7a4-5746388120de}">
      <Declaration><![CDATA[METHOD RunProgram : BOOL
VAR_INPUT
	inProgramNumber: USINT;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[//Check if I can be invoked
IF outRobotStatus = eRobotState.AutoRunning AND _step = 0 THEN
	//Robot is in the right state for Program to be called
	_programTorun := inProgramNumber;
	_step := 300; //Run Programs
	RunProgram := TRUE; //Return that it could be started
ELSE
	RunProgram := FALSE; //Return false, robot not in right state
END_IF]]></ST>
      </Implementation>
    </Method>
    <LineIds Name="ABB_IRC5">
      <LineId Id="614" Count="1" />
      <LineId Id="613" Count="0" />
      <LineId Id="15" Count="0" />
      <LineId Id="617" Count="3" />
      <LineId Id="616" Count="0" />
      <LineId Id="213" Count="1" />
      <LineId Id="217" Count="0" />
      <LineId Id="215" Count="0" />
      <LineId Id="218" Count="0" />
      <LineId Id="14" Count="0" />
      <LineId Id="9" Count="0" />
      <LineId Id="86" Count="0" />
      <LineId Id="85" Count="0" />
      <LineId Id="87" Count="1" />
      <LineId Id="670" Count="0" />
      <LineId Id="121" Count="0" />
      <LineId Id="672" Count="0" />
      <LineId Id="671" Count="0" />
      <LineId Id="720" Count="2" />
      <LineId Id="706" Count="0" />
      <LineId Id="673" Count="0" />
      <LineId Id="716" Count="0" />
      <LineId Id="707" Count="0" />
      <LineId Id="725" Count="0" />
      <LineId Id="718" Count="0" />
      <LineId Id="731" Count="0" />
      <LineId Id="730" Count="0" />
      <LineId Id="732" Count="0" />
      <LineId Id="734" Count="1" />
      <LineId Id="733" Count="0" />
      <LineId Id="723" Count="0" />
      <LineId Id="719" Count="0" />
      <LineId Id="90" Count="0" />
      <LineId Id="57" Count="0" />
      <LineId Id="93" Count="0" />
      <LineId Id="185" Count="3" />
      <LineId Id="736" Count="0" />
      <LineId Id="704" Count="0" />
      <LineId Id="713" Count="0" />
      <LineId Id="189" Count="0" />
      <LineId Id="729" Count="0" />
      <LineId Id="714" Count="0" />
      <LineId Id="190" Count="1" />
      <LineId Id="349" Count="1" />
      <LineId Id="348" Count="0" />
      <LineId Id="192" Count="0" />
      <LineId Id="351" Count="0" />
      <LineId Id="98" Count="0" />
      <LineId Id="64" Count="0" />
      <LineId Id="173" Count="2" />
      <LineId Id="196" Count="0" />
      <LineId Id="172" Count="0" />
      <LineId Id="70" Count="0" />
      <LineId Id="56" Count="0" />
      <LineId Id="58" Count="1" />
      <LineId Id="201" Count="0" />
      <LineId Id="219" Count="1" />
      <LineId Id="206" Count="0" />
      <LineId Id="254" Count="0" />
      <LineId Id="249" Count="1" />
      <LineId Id="252" Count="1" />
      <LineId Id="222" Count="0" />
      <LineId Id="221" Count="0" />
      <LineId Id="223" Count="0" />
      <LineId Id="230" Count="0" />
      <LineId Id="256" Count="0" />
      <LineId Id="325" Count="1" />
      <LineId Id="255" Count="0" />
      <LineId Id="441" Count="0" />
      <LineId Id="228" Count="0" />
      <LineId Id="231" Count="1" />
      <LineId Id="234" Count="0" />
      <LineId Id="242" Count="0" />
      <LineId Id="235" Count="1" />
      <LineId Id="243" Count="0" />
      <LineId Id="239" Count="1" />
      <LineId Id="357" Count="0" />
      <LineId Id="263" Count="1" />
      <LineId Id="245" Count="0" />
      <LineId Id="276" Count="0" />
      <LineId Id="241" Count="0" />
      <LineId Id="257" Count="2" />
      <LineId Id="261" Count="0" />
      <LineId Id="277" Count="2" />
      <LineId Id="233" Count="0" />
      <LineId Id="284" Count="0" />
      <LineId Id="299" Count="2" />
      <LineId Id="295" Count="0" />
      <LineId Id="399" Count="0" />
      <LineId Id="296" Count="0" />
      <LineId Id="306" Count="0" />
      <LineId Id="298" Count="0" />
      <LineId Id="307" Count="0" />
      <LineId Id="283" Count="0" />
      <LineId Id="226" Count="0" />
      <LineId Id="317" Count="1" />
      <LineId Id="320" Count="1" />
      <LineId Id="319" Count="0" />
      <LineId Id="322" Count="1" />
      <LineId Id="331" Count="0" />
      <LineId Id="365" Count="0" />
      <LineId Id="373" Count="0" />
      <LineId Id="400" Count="0" />
      <LineId Id="374" Count="1" />
      <LineId Id="377" Count="1" />
      <LineId Id="364" Count="0" />
      <LineId Id="267" Count="0" />
      <LineId Id="333" Count="2" />
      <LineId Id="401" Count="0" />
      <LineId Id="336" Count="3" />
      <LineId Id="402" Count="1" />
      <LineId Id="405" Count="0" />
      <LineId Id="340" Count="2" />
      <LineId Id="443" Count="0" />
      <LineId Id="397" Count="0" />
      <LineId Id="406" Count="0" />
      <LineId Id="411" Count="0" />
      <LineId Id="418" Count="0" />
      <LineId Id="698" Count="0" />
      <LineId Id="419" Count="0" />
      <LineId Id="412" Count="1" />
      <LineId Id="699" Count="0" />
      <LineId Id="415" Count="1" />
      <LineId Id="398" Count="0" />
      <LineId Id="347" Count="0" />
      <LineId Id="434" Count="2" />
      <LineId Id="440" Count="0" />
      <LineId Id="430" Count="0" />
      <LineId Id="444" Count="4" />
      <LineId Id="431" Count="0" />
      <LineId Id="450" Count="1" />
      <LineId Id="449" Count="0" />
      <LineId Id="432" Count="0" />
      <LineId Id="452" Count="0" />
      <LineId Id="460" Count="0" />
      <LineId Id="454" Count="2" />
      <LineId Id="513" Count="0" />
      <LineId Id="480" Count="0" />
      <LineId Id="457" Count="0" />
      <LineId Id="481" Count="2" />
      <LineId Id="458" Count="0" />
      <LineId Id="462" Count="0" />
      <LineId Id="464" Count="1" />
      <LineId Id="463" Count="0" />
      <LineId Id="477" Count="2" />
      <LineId Id="484" Count="3" />
      <LineId Id="433" Count="0" />
      <LineId Id="489" Count="1" />
      <LineId Id="488" Count="0" />
      <LineId Id="491" Count="3" />
      <LineId Id="496" Count="5" />
      <LineId Id="495" Count="0" />
      <LineId Id="502" Count="0" />
      <LineId Id="518" Count="3" />
      <LineId Id="517" Count="0" />
      <LineId Id="525" Count="0" />
      <LineId Id="701" Count="1" />
      <LineId Id="526" Count="6" />
      <LineId Id="534" Count="0" />
      <LineId Id="537" Count="1" />
      <LineId Id="540" Count="1" />
      <LineId Id="539" Count="0" />
      <LineId Id="533" Count="0" />
      <LineId Id="522" Count="0" />
      <LineId Id="542" Count="2" />
      <LineId Id="553" Count="0" />
      <LineId Id="546" Count="1" />
      <LineId Id="545" Count="0" />
      <LineId Id="549" Count="2" />
      <LineId Id="554" Count="1" />
      <LineId Id="645" Count="0" />
      <LineId Id="556" Count="1" />
      <LineId Id="523" Count="0" />
      <LineId Id="559" Count="0" />
      <LineId Id="700" Count="0" />
      <LineId Id="560" Count="0" />
      <LineId Id="558" Count="0" />
      <LineId Id="585" Count="3" />
      <LineId Id="591" Count="2" />
      <LineId Id="595" Count="2" />
      <LineId Id="726" Count="0" />
      <LineId Id="612" Count="0" />
      <LineId Id="599" Count="0" />
      <LineId Id="598" Count="0" />
      <LineId Id="600" Count="0" />
      <LineId Id="602" Count="0" />
      <LineId Id="727" Count="0" />
      <LineId Id="603" Count="0" />
      <LineId Id="601" Count="0" />
      <LineId Id="524" Count="0" />
      <LineId Id="437" Count="1" />
      <LineId Id="315" Count="0" />
      <LineId Id="439" Count="0" />
      <LineId Id="381" Count="0" />
      <LineId Id="387" Count="2" />
      <LineId Id="382" Count="1" />
      <LineId Id="316" Count="0" />
      <LineId Id="309" Count="0" />
      <LineId Id="384" Count="0" />
      <LineId Id="390" Count="2" />
      <LineId Id="385" Count="1" />
      <LineId Id="379" Count="1" />
      <LineId Id="421" Count="5" />
      <LineId Id="420" Count="0" />
      <LineId Id="427" Count="0" />
      <LineId Id="469" Count="3" />
      <LineId Id="476" Count="0" />
      <LineId Id="473" Count="1" />
      <LineId Id="468" Count="0" />
      <LineId Id="475" Count="0" />
      <LineId Id="504" Count="2" />
      <LineId Id="508" Count="2" />
      <LineId Id="503" Count="0" />
      <LineId Id="561" Count="0" />
      <LineId Id="563" Count="5" />
      <LineId Id="562" Count="0" />
      <LineId Id="569" Count="0" />
      <LineId Id="571" Count="5" />
      <LineId Id="570" Count="0" />
      <LineId Id="577" Count="0" />
      <LineId Id="579" Count="5" />
      <LineId Id="578" Count="0" />
      <LineId Id="511" Count="0" />
      <LineId Id="605" Count="5" />
      <LineId Id="604" Count="0" />
      <LineId Id="611" Count="0" />
      <LineId Id="269" Count="0" />
      <LineId Id="393" Count="0" />
      <LineId Id="395" Count="1" />
      <LineId Id="270" Count="0" />
      <LineId Id="272" Count="1" />
      <LineId Id="224" Count="0" />
      <LineId Id="200" Count="0" />
    </LineIds>
    <LineIds Name="ABB_IRC5._ResetSeqFault">
      <LineId Id="6" Count="2" />
      <LineId Id="5" Count="0" />
    </LineIds>
    <LineIds Name="ABB_IRC5.CycleStart">
      <LineId Id="8" Count="0" />
      <LineId Id="10" Count="1" />
      <LineId Id="14" Count="0" />
      <LineId Id="22" Count="0" />
      <LineId Id="19" Count="2" />
      <LineId Id="12" Count="0" />
    </LineIds>
    <LineIds Name="ABB_IRC5.CycleStop">
      <LineId Id="6" Count="2" />
      <LineId Id="21" Count="0" />
      <LineId Id="15" Count="3" />
      <LineId Id="5" Count="0" />
    </LineIds>
    <LineIds Name="ABB_IRC5.ResetSeq">
      <LineId Id="5" Count="0" />
    </LineIds>
    <LineIds Name="ABB_IRC5.RunProgram">
      <LineId Id="7" Count="2" />
      <LineId Id="16" Count="0" />
      <LineId Id="12" Count="3" />
      <LineId Id="5" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>