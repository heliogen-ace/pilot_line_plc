﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.9">
  <POU Name="YaskawaDrive_DS301" Id="{29ea9904-b148-4f96-a237-1abf2f9d2944}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK YaskawaDrive_DS301 IMPLEMENTS IGenericAcDrive
VAR_INPUT
END_VAR
VAR_IN_OUT
	inoProcessData : YaskawaEthercatInterface_DS301;
END_VAR
VAR_OUTPUT
END_VAR
VAR
	//Use internal variables for data transfer between Methods/Props
	// to ensure that the end user calls this Function Block
	_controlWord : ControlWord_DS301;
	_statusWord : StatusWord_DS301;
	_speedCommand : LREAL;
	_speedFeedback : LREAL;
	
	_resetRequest : BOOL;
	_resetPulseTimer : TP;
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[//Pull drive inputs in for processing
_statusWord := inoProcessData.DriveStatus.Bits;
_speedFeedback := UINT_TO_LREAL(inoProcessData.SpeedFeedback) / 100;

//Pulse timer for reset signal to ensure the drive has time to process the change of state
_resetPulseTimer(IN:= _resetRequest, PT:= T#1S, Q=> _controlWord.FaultReset);
IF _resetPulseTimer.Q THEN
	_resetRequest := FALSE;
END_IF

//Push new commands back to the drive
inoProcessData.OperationCommand.Bits := _controlWord;
inoProcessData.SpeedCommand := LREAL_TO_UINT(_speedCommand) * 100;]]></ST>
    </Implementation>
    <Property Name="Alarm" Id="{e80aa010-293c-4804-a55f-53691a8fd883}">
      <Declaration><![CDATA[PROPERTY Alarm : BOOL]]></Declaration>
      <Get Name="Get" Id="{ce735589-58f8-4faa-8501-236ae8dd2856}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[Alarm := _statusWord.Alarm;]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Property Name="AtSpeed" Id="{17d15f7f-48a7-4963-b4b1-76fd21f0c223}">
      <Declaration><![CDATA[PROPERTY AtSpeed : BOOL
]]></Declaration>
      <Get Name="Get" Id="{8f40b6b6-b36f-4740-91bc-b54b12a909cb}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[AtSpeed := _statusWord.SpeedAgree;]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Property Name="Faulted" Id="{e266b6b6-f706-4755-b560-9c78de9e0185}">
      <Declaration><![CDATA[PROPERTY Faulted : BOOL
]]></Declaration>
      <Get Name="Get" Id="{519be494-972b-4059-be07-9eb5782ba740}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[Faulted := _statusWord.Fault;]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Property Name="ReadyToRun" Id="{61da2c87-9a26-4ff2-80c8-d21222ab8755}">
      <Declaration><![CDATA[PROPERTY ReadyToRun : BOOL]]></Declaration>
      <Get Name="Get" Id="{339953a1-d6ee-4f3b-8d46-eee57ff977a2}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[ReadyToRun := _statusWord.DriveReady;]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Method Name="ResetFault" Id="{2d2dcb1c-df1e-4a86-b916-ea68d701e9dc}">
      <Declaration><![CDATA[METHOD ResetFault : BOOL
]]></Declaration>
      <Implementation>
        <ST><![CDATA[_resetRequest := TRUE;]]></ST>
      </Implementation>
    </Method>
    <Method Name="RunForward" Id="{7704d486-9dd5-4b09-9bf2-34d89a6c2c1a}">
      <Declaration><![CDATA[METHOD RunForward : BOOL
]]></Declaration>
      <Implementation>
        <ST><![CDATA[_controlWord.ForwardRun := TRUE;
_controlWord.ReverseRun := FALSE;]]></ST>
      </Implementation>
    </Method>
    <Method Name="RunReverse" Id="{f7d20a56-e625-48fe-a76d-cb0546a43927}">
      <Declaration><![CDATA[METHOD RunReverse : BOOL
]]></Declaration>
      <Implementation>
        <ST><![CDATA[_controlWord.ForwardRun := FALSE;
_controlWord.ReverseRun := TRUE;]]></ST>
      </Implementation>
    </Method>
    <Method Name="SetSpeed" Id="{f86f1707-8c42-4358-b723-bc45a3c27d02}">
      <Declaration><![CDATA[METHOD SetSpeed : BOOL
VAR_INPUT
	targetSpeed : LREAL; 
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[//Units are 0 to 100.00%, Drive parameter o1-03 = 1
IF targetSpeed >= 0 AND targetSpeed <= 100.00 THEN 
	_speedCommand := targetSpeed;
	SetSpeed := TRUE;
ELSE
	SetSpeed := FALSE;
END_IF]]></ST>
      </Implementation>
    </Method>
    <Method Name="Stop" Id="{1e2064d7-d986-4d11-b771-0372c963998d}">
      <Declaration><![CDATA[METHOD Stop : BOOL
]]></Declaration>
      <Implementation>
        <ST><![CDATA[_controlWord.ForwardRun := FALSE;
_controlWord.ReverseRun := FALSE;]]></ST>
      </Implementation>
    </Method>
    <LineIds Name="YaskawaDrive_DS301">
      <LineId Id="66" Count="2" />
      <LineId Id="96" Count="0" />
      <LineId Id="88" Count="0" />
      <LineId Id="90" Count="2" />
      <LineId Id="89" Count="0" />
      <LineId Id="97" Count="0" />
      <LineId Id="69" Count="1" />
      <LineId Id="51" Count="0" />
    </LineIds>
    <LineIds Name="YaskawaDrive_DS301.Alarm.Get">
      <LineId Id="2" Count="0" />
    </LineIds>
    <LineIds Name="YaskawaDrive_DS301.AtSpeed.Get">
      <LineId Id="2" Count="0" />
    </LineIds>
    <LineIds Name="YaskawaDrive_DS301.Faulted.Get">
      <LineId Id="2" Count="0" />
    </LineIds>
    <LineIds Name="YaskawaDrive_DS301.ReadyToRun.Get">
      <LineId Id="2" Count="0" />
    </LineIds>
    <LineIds Name="YaskawaDrive_DS301.ResetFault">
      <LineId Id="4" Count="0" />
    </LineIds>
    <LineIds Name="YaskawaDrive_DS301.RunForward">
      <LineId Id="4" Count="1" />
    </LineIds>
    <LineIds Name="YaskawaDrive_DS301.RunReverse">
      <LineId Id="5" Count="0" />
      <LineId Id="4" Count="0" />
    </LineIds>
    <LineIds Name="YaskawaDrive_DS301.SetSpeed">
      <LineId Id="13" Count="0" />
      <LineId Id="9" Count="0" />
      <LineId Id="7" Count="1" />
      <LineId Id="11" Count="1" />
      <LineId Id="10" Count="0" />
    </LineIds>
    <LineIds Name="YaskawaDrive_DS301.Stop">
      <LineId Id="5" Count="0" />
      <LineId Id="4" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>