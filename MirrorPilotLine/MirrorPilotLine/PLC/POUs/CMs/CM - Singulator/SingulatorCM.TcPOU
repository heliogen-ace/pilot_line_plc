﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.9">
  <POU Name="SingulatorCM" Id="{d60166e7-f53e-445d-81e9-af79b3dd32cf}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK SingulatorCM
VAR_INPUT
	inLimitSwitchPos: BOOL;
	inLimitSwitchNeg: BOOL;
	inHomePosSwitch: BOOL;
	inSpacerSensor: BOOL;
	inResetFault: BOOL;
END_VAR
VAR_INPUT PERSISTENT
	cfg: SingulatorCMcfg;
END_VAR
VAR_OUTPUT
	outState: eSingulatorState;
	axisHomed: BOOL;
	outFault: BOOL;
	outFaultMsg: STRING;
	outNumSpacers: INT;
END_VAR
VAR_IN_OUT
	inoAxisRef: Axis_Ref;
END_VAR
VAR
	_axis: ServoMotorPTP;
	_upBtn: K50P;
	_dnBtn: K50P;
	_step: INT; //Step Sequencer Index
	_stepTimer: TON;
	_oldStep: INT;
	_PosLimit: BOOL;
	_NegLimit: BOOL;
	_ReturnStep: INT; //Step for fault sequencer to return too
	_ButtonTimer: TON;
	_cfgK50P_SolidColors: K50P_SolidColors;
	_cfgK50P_FlashingColors: K50P_FlashingColors;
	_cfgK50P_ChaseColors: K50P_ChaseColors;
	MMPERSPACER: LREAL := 3.0;
	TOTALTRAVEL: LREAL := 840.0;
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////									Singulator CM for Mirror Pilot Line											////
////																												////
////									Work in Progress 3/9/2022 - O Ghalebi										////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Singulator Servo Motor PTP Call
_axis(inoAxisRef := inoAxisRef,inLimitSwitchPos:=inLimitSwitchPos,inLimitSwitchNeg:=inLimitSwitchNeg,inHomePosSwitch:=inHomePosSwitch);

//Need to handle axis faults!
IF inoAxisRef.Status.Error THEN
	_step := 9010; //Fault state
END_IF

//OR Soft and Physical Limits - _PosLimit and _NegLimit are TRUE for limit exceeded/hit, i.e. opposite of an NC switch
_PosLimit := inoAxisRef.Status.SoftLimitMaxExceeded OR NOT inLimitSwitchPos;
_NegLimit := inoAxisRef.Status.SoftLimitMinExceeded OR NOT inLimitSwitchNeg;

//ButtonCall

_upBtn();
_dnBtn();

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////									Singulator State Determination												////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

CASE _step OF
	0: outState := eSingulatorState.NeedsAxisHome;
		_upBtn.Output(_cfgK50P_SolidColors.Red);
		_dnBtn.Output(_cfgK50P_SolidColors.Red);
		
	100..199: outState := eSingulatorState.HomingAxis;
		_upBtn.Output(_cfgK50P_ChaseColors.Amber);
		_dnBtn.Output(_cfgK50P_ChaseColors.Amber);
	
	200..299: outState := eSingulatorState.HomingSpacers;
		_upBtn.Output(_cfgK50P_ChaseColors.Blue);
		_dnBtn.Output(_cfgK50P_ChaseColors.Blue);
		
	300..399: outState := eSingulatorState.Idle;
		_upBtn.Output(_cfgK50P_FlashingColors.Green);
		_dnBtn.Output(_cfgK50P_FlashingColors.Green);
		
	400..499: outState := eSingulatorState.Manual;
		//Light Status set inside step
	
	500..599: outState := eSingulatorState.Auto;
		//Light Status set inside step
	
	600..699: outState := eSingulatorState.Empty;
		_upBtn.Output(_cfgK50P_SolidColors.Blue);
		_dnBtn.Output(_cfgK50P_SolidColors.Blue);
		
	9000..9999: outstate := eSingulatorState.Faulted;
		_upBtn.Output(_cfgK50P_FlashingColors.Red);
		_dnBtn.Output(_cfgK50P_FlashingColors.Red);
		
	ELSE //Unknown State
		_upBtn.Output(_cfgK50P_SolidColors.Off);
		_dnBtn.Output(_cfgK50P_SolidColors.Off);
END_CASE

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////										Manual Mode Trigger														////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

_ButtonTimer(in:=_upBtn.inButton AND _dnBtn.inButton,PT:=cfg.ButtonPressTime);

//Allow access into manual mode from Idle, Auto, Empty, 
CASE _step OF
	300..399,500..699: 
		IF _ButtonTimer.Q THEN
			_step := 400;
		END_IF
END_CASE

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////									Step Sequencer to handle methods 											////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

_stepTimer(IN:=_step > 0 AND _step = _oldStep,PT:=T#30D);
IF _step <> _oldStep THEN
	_oldStep := _step;
END_IF

CASE _step OF
	////////////////////////////////////////////////////////////////
	////					Needs Axis Home						////
	////////////////////////////////////////////////////////////////
	
	0: //Needs Servo Homed (Startup State)
		//Wait for HomeAxis Method Call
		axisHomed := FALSE;
		
	////////////////////////////////////////////////////////////////
	////					Homing Axis							////
	////////////////////////////////////////////////////////////////
		
	100: //Power On
	
		IF inoAxisRef.Status.Error THEN //If faulted call a reset first
			Reset();
		END_IF
		
		_axis.PowerOn();
		
		IF _axis.outPwr.Active AND NOT _axis.outPwr.Error AND _stepTimer.ET > T#500ms THEN
			_step := 110;
		END_IF
		
		IF _stepTimer.ET > T#1S THEN
			_axis.PowerOff();
			_step := 9000; //Failed to power on axis
		END_IF
		
	110: //Home Axis
	
		_axis.Home();
		
		IF _axis.outHome.Done THEN
			axisHomed := TRUE;
			_step := 200; //Homing Done, Home Spacers
		END_IF	
	
	////////////////////////////////////////////////////////////////
	////					Homing Spacers						////
	////////////////////////////////////////////////////////////////
	
	200: //Power On
		_axis.PowerOn();
		
		IF _axis.outPwr.Active AND NOT _axis.outPwr.Error THEN
			_step := 210;
		END_IF
		
		IF _stepTimer.ET > T#1S THEN
			_axis.PowerOff();
			_step := 9000; //Failed to power on axis
		END_IF
		
	210: //You are powered - check to see if spacer fork sensor blocked, if true, lower until it isn't
	
		IF inSpacerSensor THEN
			_axis.Jog(Fwd:=FALSE,Rev:=TRUE,Velocity:=cfg.HomeSpeed,Accel:=0,Decel:=0,Jerk:=0);
		ELSE
			_axis.Jog(Fwd:=FALSE,Rev:=FALSE,Velocity:=cfg.HomeSpeed,Accel:=0,Decel:=0,Jerk:=0);
			_step := 220;
		END_IF
		
		//Add Error handling for failed move
		
	220: //Jog up to find spacer sensor
		_axis.Jog(Fwd:=NOT inSpacerSensor,Rev:=FALSE,Velocity:=cfg.HomeSpeed,Accel:=0,Decel:=0,Jerk:=0);
		IF inSpacerSensor AND _axis.outJog.Done THEN
			_axis.Jog(Fwd:=FALSE,Rev:=FALSE,Velocity:=cfg.HomeSpeed,Accel:=0,Decel:=0,Jerk:=0);
			_step := 300;
		END_IF
	
		IF _PosLimit OR _axis.outJog.CommandAborted THEN //Soft limit triggers command aborted, possible to blow through soft limit so _PosLimit is also there
			_axis.Jog(Fwd:=FALSE,Rev:=FALSE,Velocity:=cfg.HomeSpeed,Accel:=0,Decel:=0,Jerk:=0);
			_step := 600; //Empty!
		END_IF
		
(*		//Add Error handling for failed move
		
	230: //Crawl down until not spacer sensor
		_axis.Jog(Fwd:=FALSE,Rev:=inSpacerSensor,Velocity:=cfg.HomeCrawlSpeed,Accel:=0,Decel:=0,Jerk:=0);
		IF NOT inSpacerSensor THEN
			_axis.Jog(Fwd:=FALSE,Rev:=FALSE,Velocity:=cfg.HomeSpeed,Accel:=0,Decel:=0,Jerk:=0);
			_step := 240;
		END_IF
		
		//Add Error handling for failed move
		
	240: //Crawl up to find spacer sensor again
		_axis.Jog(Fwd:=NOT inSpacerSensor,Rev:=FALSE,Velocity:=cfg.HomeCrawlSpeed,Accel:=0,Decel:=0,Jerk:=0);
		IF inSpacerSensor AND _axis.outJog.Done THEN
			_step := 300;
		END_IF
		
		//Add Error handling for failed move
	*)		
	////////////////////////////////////////////////////////////////
	////						Idle							////
	////////////////////////////////////////////////////////////////	
	
	300: 
		_axis.PowerOff();
		_step := 310;

	////////////////////////////////////////////////////////////////
	////						Manual							////
	////////////////////////////////////////////////////////////////	

	400: //Power Up
		_axis.PowerOn();
		
		IF _axis.outPwr.Active AND NOT _axis.outPwr.Error THEN
			_step := 410;
		END_IF
		
		IF _stepTimer.ET > T#1S THEN
			_axis.PowerOff();
			_step := 9000; //Failed to power on axis
		END_IF


	410: //All actions happen in this step
		IF _upBtn.inButton AND _dnBtn.inButton THEN
			//Handle Both Button Push
			_axis.Jog(Fwd:=FALSE,Rev:=FALSE,Velocity:=0,Accel:=0,Decel:=0,Jerk:=0);
			_upBtn.Output(_cfgK50P_ChaseColors.Magenta);
			_dnBtn.Output(_cfgK50P_ChaseColors.Magenta);
		
			IF NOT inSpacerSensor THEN
				//OK To Exit Manual
				IF _ButtonTimer.Q THEN
					_step:= 200; //Home Spacers
				END_IF
			ELSE
				//OK To Exit Manual
				
			END_IF
		ELSIF _upBtn.inButton THEN
			//Up Button Pressed
			_axis.Jog(Fwd:=TRUE,Rev:=FALSE,Velocity:=cfg.JogSpeed,Accel:=0,Decel:=0,Jerk:=0);
			
			//Light Handler
			IF _axis.outJog.Busy THEN
				_upBtn.Output(_cfgK50P_ChaseColors.Magenta);
				_dnBtn.Output(_cfgK50P_SolidColors.Off);
			ELSIF _axis.outJog.CommandAborted THEN
				//Hit Soft Limit while jogging
				_upBtn.Output(_cfgK50P_SolidColors.Red);
				_dnBtn.Output(_cfgK50P_SolidColors.Off);
			ELSE
				//Not Jogging
				_upBtn.Output(_cfgK50P_SolidColors.Off);
				_dnBtn.Output(_cfgK50P_SolidColors.Off);
			END_IF
			
		ELSIF _dnBtn.inButton THEN
			//Dn Button Pressed
			_axis.Jog(Fwd:=FALSE,Rev:=TRUE,Velocity:=cfg.JogSpeed,Accel:=0,Decel:=0,Jerk:=0);
			
			//Light Handler
			IF _axis.outJog.Busy THEN
				_upBtn.Output(_cfgK50P_SolidColors.Off);
				_dnBtn.Output(_cfgK50P_ChaseColors.Magenta);
			ELSIF _axis.outJog.CommandAborted THEN
				//Hit Soft Limit while jogging
				_upBtn.Output(_cfgK50P_SolidColors.Off);
				_dnBtn.Output(_cfgK50P_SolidColors.Red);
			ELSE
				//Not Jogging
				_upBtn.Output(_cfgK50P_SolidColors.Off);
				_dnBtn.Output(_cfgK50P_SolidColors.Off);
			END_IF
			
		ELSE
			//No Buttons Pressed
			_axis.Jog(Fwd:=FALSE,Rev:=FALSE,Velocity:=0,Accel:=0,Decel:=0,Jerk:=0);
			_upBtn.Output(_cfgK50P_SolidColors.Magenta);
			_dnBtn.Output(_cfgK50P_SolidColors.Magenta);
		END_IF
		
	////////////////////////////////////////////////////////////////
	////						Auto Mode						////
	////////////////////////////////////////////////////////////////
	
	500: //Entry State
		IF _axis.PowerOn() THEN
			_step := 510;
		END_IF
	
	510: //Wait For Spacer to be Removed 
			_upBtn.Output(_cfgK50P_SolidColors.Green);
			_dnBtn.Output(_cfgK50P_SolidColors.Green);
		IF NOT inSpacerSensor THEN
			_step := 520;
		END_IF
	
	520: //Spacer Removed - feed new spacer
		IF _stepTimer.ET > cfg.FallingEdgeTime  THEN //Falling edge timer
			_axis.Jog(Fwd:=NOT inSpacerSensor,Rev:=FALSE,Velocity:=cfg.FeedSpeed,Accel:=0,Decel:=0,Jerk:=0);
			_upBtn.Output(_cfgK50P_ChaseColors.Green);
			_dnBtn.Output(_cfgK50P_ChaseColors.Green);
			IF inSpacerSensor THEN
				_axis.Jog(Fwd:=FALSE,Rev:=FALSE,Velocity:=cfg.FeedSpeed,Accel:=0,Decel:=0,Jerk:=0);
				_step := 510;
			END_IF
			
			IF _PosLimit OR _axis.outJog.CommandAborted THEN
				_axis.Jog(Fwd:=FALSE,Rev:=FALSE,Velocity:=cfg.FeedSpeed,Accel:=0,Decel:=0,Jerk:=0);
				_step := 600; //Empty!
			END_IF
		END_IF	
		
	////////////////////////////////////////////////////////////////
	////						Empty State						////
	////////////////////////////////////////////////////////////////
		
	600: //Entry State
		_axis.PowerOff();
		_step := 610;
		

	
	////////////////////////////////////////////////////////////////
	////						Faulted							////
	////////////////////////////////////////////////////////////////	
	
	9000: //Failed to Power on Axis
		outFault := TRUE;
		outFaultMsg := '9000: Failed to turn on Axis for Axis Homing';
		_ReturnStep := 0;
		IF inResetFault THEN
			Reset();
		END_IF	
		
	9001: //Failed to Power on Axis
		outFault := TRUE;
		outFaultMsg := '9001: Failed to turn on Axis for Spacer Homing';
		_ReturnStep := 100;
		
		IF inResetFault THEN
			Reset();
		END_IF	
		
	9010: //Axis Faulted
		outFault := TRUE;
		outFaultMsg := CONCAT('9010: Axis Faulted - Error ID: ',UDINT_TO_STRING(inoAxisRef.Status.ErrorID));
		_ReturnStep := 0;
		
		IF inoAxisRef.Status.Error = FALSE THEN //Axis was reset externally
			_step := 0;
		END_IF
		
		IF inResetFault THEN
			Reset();
		END_IF
END_CASE

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////										Spacer Count Calculation												////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

IF axisHomed THEN
	IF MMPERSPACER <> 0 THEN
		outNumSpacers := LREAL_TO_INT((TOTALTRAVEL - inoAxisRef.NcToPlc.ActPos) / MMPERSPACER);
	ELSE
		outNumSpacers := 0;
	END_IF
	
ELSE
	outNumSpacers := 0;
END_IF]]></ST>
    </Implementation>
    <Method Name="Auto" Id="{b4620135-49b3-4ea0-a84f-550a089ca59c}">
      <Declaration><![CDATA[METHOD Auto : BOOL
VAR_INPUT
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[//Check if I can be invoked
IF  _step >= 300 AND _step <= 399 THEN
	_step := 500;
	Auto := TRUE; //Return that it could be started
ELSE
	Auto :=FALSE; //Return false, robot not in right state
END_IF]]></ST>
      </Implementation>
    </Method>
    <Method Name="HomeAxis" Id="{b8efd729-ee46-4e94-b333-d651892a62c9}">
      <Declaration><![CDATA[METHOD HomeAxis : BOOL
VAR_INPUT
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[_step := 100;]]></ST>
      </Implementation>
    </Method>
    <Method Name="HomeSpacers" Id="{a216f0fc-5d8b-4e05-a5d8-52625df28d31}">
      <Declaration><![CDATA[METHOD HomeSpacers : BOOL
VAR_INPUT
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[_step := 200;
]]></ST>
      </Implementation>
    </Method>
    <Method Name="Idle" Id="{0aa43023-1afe-4b1a-a47b-1d83cb2c11af}">
      <Declaration><![CDATA[METHOD Idle : BOOL
VAR_INPUT
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[//Can always be invoked
	_step := 300;
]]></ST>
      </Implementation>
    </Method>
    <Method Name="Manual" Id="{8c6fab4c-78ca-40e0-963f-e5214a31d47e}">
      <Declaration><![CDATA[METHOD Manual : BOOL
VAR_INPUT
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[	_step := 400;
]]></ST>
      </Implementation>
    </Method>
    <Method Name="Reset" Id="{ca88bb01-3637-41e3-a1ba-4efaa1677a63}">
      <Declaration><![CDATA[METHOD Reset : BOOL
VAR_INPUT
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[inResetFault := FALSE;
outFault := FALSE;
outFaultMsg := '';
_axis.Reset();
_step := _ReturnStep;]]></ST>
      </Implementation>
    </Method>
    <LineIds Name="SingulatorCM">
      <LineId Id="690" Count="16" />
      <LineId Id="1160" Count="0" />
      <LineId Id="707" Count="0" />
      <LineId Id="1162" Count="0" />
      <LineId Id="1161" Count="0" />
      <LineId Id="1163" Count="1" />
      <LineId Id="708" Count="69" />
      <LineId Id="1116" Count="0" />
      <LineId Id="778" Count="5" />
      <LineId Id="1021" Count="4" />
      <LineId Id="784" Count="11" />
      <LineId Id="1062" Count="0" />
      <LineId Id="796" Count="2" />
      <LineId Id="1115" Count="0" />
      <LineId Id="799" Count="211" />
      <LineId Id="1015" Count="3" />
      <LineId Id="1011" Count="3" />
      <LineId Id="45" Count="0" />
      <LineId Id="1103" Count="2" />
      <LineId Id="1102" Count="0" />
      <LineId Id="1107" Count="0" />
      <LineId Id="1106" Count="0" />
      <LineId Id="1117" Count="1" />
      <LineId Id="1120" Count="1" />
      <LineId Id="1119" Count="0" />
      <LineId Id="1108" Count="0" />
      <LineId Id="1110" Count="1" />
      <LineId Id="1109" Count="0" />
    </LineIds>
    <LineIds Name="SingulatorCM.Auto">
      <LineId Id="6" Count="5" />
      <LineId Id="5" Count="0" />
    </LineIds>
    <LineIds Name="SingulatorCM.HomeAxis">
      <LineId Id="12" Count="0" />
    </LineIds>
    <LineIds Name="SingulatorCM.HomeSpacers">
      <LineId Id="17" Count="0" />
      <LineId Id="5" Count="0" />
    </LineIds>
    <LineIds Name="SingulatorCM.Idle">
      <LineId Id="6" Count="0" />
      <LineId Id="8" Count="0" />
      <LineId Id="5" Count="0" />
    </LineIds>
    <LineIds Name="SingulatorCM.Manual">
      <LineId Id="9" Count="0" />
      <LineId Id="5" Count="0" />
    </LineIds>
    <LineIds Name="SingulatorCM.Reset">
      <LineId Id="6" Count="2" />
      <LineId Id="12" Count="0" />
      <LineId Id="5" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>